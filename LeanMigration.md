# How to update

* Copy `code`, `contrib`, `include` to `assimp-lean`
* Add `GitVersion` and `GitBranch` to `Version.cpp`
* Fix `CMakeLists.txt` of `zlib`
* Remove `gtest` from `contrib`
* Remove `contrib` from `contrib/zlib`
